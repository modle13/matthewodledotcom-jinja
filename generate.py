import jinja2
from data import section_data, head_data

def render_jinja_html(template_loc, file_name, **context):

    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(template_loc + '/')
    ).get_template(file_name).render(context)

template_path = 'app/templates'

def render_portfolio_sections():
    rendered_sections = []
    for entry in section_data:
        section = render_jinja_html(template_path, 'portfolio_section.html', data=entry)
        rendered_sections.append(section)
    return rendered_sections

portfolio_sections = render_portfolio_sections()

print(portfolio_sections)

portfolio = render_jinja_html(template_path, 'portfolio.html', data=portfolio_sections)
head = render_jinja_html(template_path, 'head.html', data=head_data)
navigation = render_jinja_html(template_path, 'navigation.html')
intro = render_jinja_html(template_path, 'intro.html')

index_contents = {
    'head': head,
    'navigation': navigation,
    'intro': intro,
    'portfolio': portfolio
}

generated = render_jinja_html(template_path, 'index.html', data=index_contents)
print (generated)
f = open('index.html', 'w+')
head = f.write(generated)
f.close()
 
